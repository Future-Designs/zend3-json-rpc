<?php
/**
 * Created by PhpStorm.
 * User: j.lohse
 * Date: 03.11.17
 * Time: 17:48
 */

namespace FuDe\JsonRpc\Controller;

use Zend\Json\Server\Server;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Json\Server\Smd;

/**
 * Class RpcController
 * @package FuDe\JsonRpc\Controller
 */
class RpcController extends AbstractActionController
{
    public function rpcAction()
    {
        $server = new Server();
        $server->setClass('FuDe\JsonRpc\Services\Calculator');

        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            // Indicate the URL endpoint, and the JSON-RPC version used:
            $server->setTarget('/json-rpc.php')
                ->setEnvelope(Smd::ENV_JSONRPC_2);

            // Grab the SMD
            $smd = $server->getServiceMap();

            // Return the SMD to the client
            header('Content-Type: application/json');
            echo $smd;
            return;
        }

        $server->handle();
    }
}