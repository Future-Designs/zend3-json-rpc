<?php

namespace FuDe\JsonRpc;

use FuDe\JsonRpc\Controller\RpcController;
use FuDe\JsonRpc\Controller\RpcControllerFactory;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'rpc' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/rpc',
                    'defaults' => [
                        'controller' => RpcController::class,
                        'action'     => 'rpc',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            RpcController::class => RpcControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
